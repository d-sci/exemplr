#' @importFrom methods new setRefClass
NULL

#' @title Reference Class Representing a Dog
#' @description A dog has a name and a hair color.
#' @field name First or full name of the dog
#' @field hair Hair color of the dog
#' @examples
#' d <- Dog("Rover", "black")
#' d$hair
#' d$set_hair("red")
#' d$hair
#' @seealso [Person]
#' @export Dog
#' @export
Dog <- setRefClass("Dog",
  fields = list(
    name = "character",
    hair = "character"
  ),
  
  methods = list(
    initialize = function(name = NA_character_, hair = NA_character_) {
      'Create a new dog object'
      .self$name <- name
      .self$hair <- hair
      .self$greet()
    },
    
    set_hair = function(val) {
      '
      @description Change hair color.
      @param       val New hair color.
      @return      Old hair color, invisibly.
      '
      old <- .self$hair
      .self$hair <- val
      invisible(old)
    },

    greet = function() {
      'Say hi'
      cat(paste0("Woof, my name is ", .self$name, ".\n"))
    }
  )
)


#' @title Case Sensitivity Test
#' @description 
#' When you run [devtools::document()] in the R console, the ordering of RC
#' methods is case insensitive. When you run documentation in the build pane
#' (e.g. via `Ctrl+Shift+D`), the ordering is case sensitive.
#' @export CaseSensitivity
#' @export
CaseSensitivity <- setRefClass("CaseSensitivity",
  methods = list(
    aa = function(){
      'If case insensitive, aa will come before AZ'
      return("insensitive")
    },
    AZ = function(){
      'If case sensitive, AZ will come before aa'
      return("sensitive")
    }
  ))