#' @title Add Two Numbers
#' @description Exactly what it sounds like.
#' @details Technically, the 'numbers' can be vectors.
#' @param a,b Numbers to add
#' @param slowly If TRUE, take a long time to return
#' @return The sum of a and b
#' @examples
#' add(2, 2)
#' 
#' \dontrun{
#' add(2, 2, slowly = TRUE)
#' }
#' @export
add <- function(a, b, slowly = FALSE) {
  if (slowly) {Sys.sleep(10)}
  return(a + b)
}

#' Multiply Two Numbers
#'
#' Also exactly what it sounds like.
#'
#' Note that the title, description, and details tags can be inferred from the
#' first, second, and subsequent pargraphs of untaggged comments. It's purely
#' a matter of style whether you prefer to be explicit.
#' 
#' @param a,b Numbers to multiply
#' @return Product of a and b
#' @export
multiply <- function(a, b) {
  return(a * b)
}
