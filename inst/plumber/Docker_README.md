To run the plumber application with Docker (locally)
First, navigate to `/inst/plumber` within wherever you cloned the package source to:
```
cd path/to/exemplr/inst/plumber
```

To build the docker image and call it "exemplr" with the tag "1.0":
```
docker image build -t exemplr:1.0 .
```

Run the image locally. The tags below mean:  

* `--rm` will remove the container once you're done with it
* `-p 8000:8000` publishes the port 8000 on the docker to 8000 on your host machine
* `-it` will bring up the R terminal in the docker on your command line so you can see what's going on
* `--mount type=bind,source="$(pwd)",target=/app` mounts your current directory in your host machine to the `/app` directory on the docker
* `-w /app` sets the working directory in the docker to `/app`. Combined with the above and the way the app works, this means that when the app saves an RDS (in response to a request to /asyncJob) it will save it to `/inst/plumber` in your host machine.
```
docker container run --rm -p 8000:8000 -it --mount type=bind,source="$(pwd)",target=/app -w /app exemplr:1.0
```

Use Ctrl-C to kill the process once you're done playing around.