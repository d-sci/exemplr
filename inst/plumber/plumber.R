# This is a Plumber API. You can run the API by clicking
# the 'Run API' button above.
#
# Find out more about building APIs with Plumber here:
#
#    https://www.rplumber.io/
#

#* @apiTitle Plumber Example API

## Basic examples --------------------------------------------------------------

#* Echo back the input
#* @param msg The message to echo
#* @get /echo
function(msg = "") {
    list(msg = paste0("The message is: '", msg, "'"))
}

#* Plot a histogram
#* @png
#* @get /plot
function() {
    rand <- rnorm(100)
    hist(rand)
}



## Long task example -----------------------------------------------------------

# Simulate a long-running job with an early return:
# longTask() artificially takes a long time (length = value of first arg in seconds).
# If you hit /asyncJob?a=5, it will generate a taskName, then send a=5 and the
# taskName to longTask(), but do so with a fork via mcparallel.
# The endpoint will immediately return the taskName and job pid (so you could
# check up on it later or whatever) while the longTask() is still actually
# running (as proof, it saves an RDS including start/end times).

library(exemplr)

# Global variable for task ids
latestTask <- 0

#* Add one to a number, after a fashion
#* @param t Number
#* @get /asyncJob
asyncJob <- function(t) {
  t <- as.integer(t)
  latestTask <<- latestTask + 1 # increment global task counter
  taskName <- sprintf("task%d", latestTask)
  job <- parallel::mcparallel(longTask(t, taskName), name = taskName)
  return(list(taskName = taskName, pid = job$pid))
}

longTask <- function(t, taskName) {
  startTime <- Sys.time()
  Sys.sleep(t)     # artificially lengthy
  result <- t %>% abra() %>% cadabra() %>% alakazam() # dummy job (using package code)
  endTime <- Sys.time()
  output <- list(result = result, start = startTime, end = endTime)
  outfile <- sprintf("Results_%s.rds", taskName)
  saveRDS(output, outfile)
  return(result)
}


