% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/basics.R
\name{multiply}
\alias{multiply}
\title{Multiply Two Numbers}
\usage{
multiply(a, b)
}
\arguments{
\item{a, b}{Numbers to multiply}
}
\value{
Product of a and b
}
\description{
Also exactly what it sounds like.
}
\details{
Note that the title, description, and details tags can be inferred from the
first, second, and subsequent pargraphs of untaggged comments. It's purely
a matter of style whether you prefer to be explicit.
}
